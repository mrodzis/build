#!/bin/sh
### BEGIN INIT INFO
# Provides:          sade
# Required-Start:    $local_fs $network $named $time $syslog
# Required-Stop:     $local_fs $network $named $time $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Description:       XML database for SADE
### END INIT INFO

STARTSCRIPT="/opt/sade/bin/startup.sh"
STOPSCRIPT="/opt/sade/bin/shutdown.sh"
RUNAS="sade"

PIDFILE=/var/run/sade.pid
LOGFILE=/var/log/sade.log

start() {
  #if [ -f /var/run/$PIDNAME ] && kill -0 $(cat /var/run/$PIDNAME); then
  #  echo 'Service already running' >&2
  #  return 1
  #fi
  #rm -f $PIDFILE
  #touch $PIDFILE
  #chown $RUNAS $PIDFILE
  touch $LOGFILE
  chown $RUNAS $LOGFILE
  echo 'Starting service…' >&2
  local CMD="$STARTSCRIPT &> \"$LOGFILE\" & echo \$!"
  su --shell=/bin/bash -c "$CMD" $RUNAS > "$PIDFILE"
  echo 'Service started' >&2
}

stop() {
  #if [ ! -f "$PIDFILE" ]; then
  #  echo 'Service not running' >&2
  #  return 1
  #fi
  echo 'Stopping service…' >&2
  #kill -15 $(cat "$PIDFILE") && rm -f "$PIDFILE"
  local CMD="$STOPSCRIPT &> \"$LOGFILE\""
  su --shell=/bin/bash -c "$CMD" $RUNAS
  echo 'Service stopped' >&2
}

status() {
  cat $LOGFILE | tail
  date
}

case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  restart)
    stop
    start
    ;;
  status)
    status
    ;;
  *)
    echo "Usage: $0 {start|stop|restart|status}"
esac
