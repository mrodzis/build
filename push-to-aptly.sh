#!/bin/bash
# pushes and publishes artifacts at aptly
# made for gitlab ci

echo "Pushing ${DEBFILE} as sade-${CI_JOB_NAME}-${CI_JOB_ID} to ${APTLY}"

curl -u ci:${APTLY_PW} -X POST -F file=@${DEBFILE} ${APTLY}/files/sade-${CI_JOB_NAME}-${CI_JOB_ID}
curl -u ci:${APTLY_PW} -X POST ${APTLY}/repos/indy-snapshots/file/sade-${CI_JOB_NAME}-${CI_JOB_ID}
curl -u ci:${APTLY_PW} -X PUT -H 'Content-Type: application/json' --data '{}' "${APTLY}/publish/:./indy"
