<?xml version="1.0" encoding="UTF-8"?>
<project basedir="." name="SADE" default="build" xmlns:ivy="antlib:org.apache.ivy.ant"
    xmlns:git="antlib:com.rimerosolutions.ant.git">

    <description> builds a generic version of SADE, customizable by projects via own parameter
        injection. built packages should be moved to autodeploy directory by their targets! </description>

    <property file="generic.build.properties" />
    <property name="ivy.jardir" value="${basedir}/lib"/>

    <tstamp>
        <format property="timestamp" pattern="yyyyMMddHHmmss" unit="hour"/>
    </tstamp>

    <target name="build" depends="build-deb-generic, build-deb-project"/>

    <target name="exist-git" depends="ant-dependencies">
        <git repo="${exist.git}" branch="tags/${exist.tag}" dest="${exist.dest}"/>
    </target>
    <target name="exist-build-conf" depends="exist-git, junit-ant-patch">
        <echo message="configuring eXist"/>
        <echo message="patching lucene inlining"/>
        <patch patchfile="${resources.dir}/lucene-inline.patch"
            originalfile="${exist.dest}/extensions/indexes/lucene/src/org/exist/indexing/lucene/DefaultTextExtractor.java"
            ignorewhitespace="true" failonerror="false"/>

        <copy file="${exist.dest}/build.properties" tofile="${exist.dest}/local.build.properties" />
        <replace file="${exist.dest}/local.build.properties" token="dashboard,shared,eXide,monex"
            value="dashboard,shared,eXide,monex,functx,fundocs,markdown"/>
    </target>
    <target name="exist-build" depends="exist-build-conf">
        <java classname="org.apache.tools.ant.launch.Launcher" fork="true">
            <arg value="-Dant.home=${exist.dest}/tools/ant"/>
            <arg value="-Dant.library.dir=${exist.dest}/tools/ant/lib"/>
            <arg value="-Divy.default.ivy.user.dir=${basedir}/ivy"/>
            <arg
                value="-Djavax.xml.transform.TransformerFactory=org.apache.xalan.processor.TransformerFactoryImpl"/>
            <arg value="-f"/>
            <arg value="${exist.dest}/build.xml"/>
            <arg value="all"/>
            <arg value="dist"/>
            <classpath>
                <pathelement location="${exist.dest}/tools/ant/lib/ant-launcher-1.10.1.jar"/>
                <!--pathelement path="${java.class.path}"/-->
            </classpath>
        </java>
    </target>
    <target name="exist-dist-conf" depends="exist-build">
        <!-- enable autostart scripts -->
        <replace file="${exist.dist}/conf.xml">
            <replacetoken>&lt;!--&lt;trigger
                class="org.exist.collections.triggers.XQueryStartupTrigger"/&gt;--&gt;</replacetoken>
            <replacevalue>&lt;trigger
                class="org.exist.collections.triggers.XQueryStartupTrigger"/&gt;</replacevalue>
        </replace>

        <!-- set backup settings -->
        <replace file="${exist.dist}/conf.xml">
            <replacetoken>&lt;/scheduler&gt;</replacetoken>
            <replacevalue>&lt;job type="system" name="check1"
              class="org.exist.storage.ConsistencyCheckTask"
              cron-trigger="0 0 23 * * ?"&gt;
              &lt;parameter name="output" value="export"/&gt;
              &lt;parameter name="backup" value="yes"/&gt;
              &lt;parameter name="incremental" value="no"/&gt;
              &lt;parameter name="incremental-check" value="no"/&gt;
              &lt;parameter name="zip" value="yes"/&gt;
              &lt;parameter name="max" value="2"/&gt;
          &lt;/job&gt;
       &lt;/scheduler&gt;</replacevalue>
        </replace>

    </target>

    <target name="prepare-dist-dir" depends="exist-dist-conf">
        <delete dir="${build.dir}/sade"/>
        <!-- get updated xars to autodeploy dir -->
        <delete dir="${exist.dist}/autodeploy"/>
        <mkdir dir="${exist.dist}/autodeploy"/>
        <move2autodeploy source="${exist.dest}/autodeploy/"/>
        <!-- move eXist to the SADE path -->
        <mkdir dir="${build.dir}/sade"/>
        <move file="${exist.dist}" tofile="${build.dir}/sade" overwrite="true"/>

        <!-- undo changes to tracked files, so git pull is possible…-->
        <patch reverse="true" patchfile="${resources.dir}/lucene-inline.patch"
            originalfile="${exist.dest}/extensions/indexes/lucene/src/org/exist/indexing/lucene/DefaultTextExtractor.java"
            ignorewhitespace="true" failonerror="false"/>

    </target>

    <!-- apply web.xml patch for dashboard, otherwise it will not be visible -->
    <target name="dashboard-fix" depends="sade">
        <ant antfile="${build.dir}/sade/extensions/betterform/build.xml" target="patchWebXml" inheritAll="false"/>
    </target>
    <target name="junit-ant-patch" depends="exist-git">
        <patch  patchfile="${resources.dir}/ant-junit-basedir.patch" dir="${exist.dest}" strip="1"/>
    </target>

    <!-- SADE -->
    <target name="sade" depends="prepare-dist-dir">
        <git repo="${sade.git}" branch="${sade.branch}" dest="${sade.dest}"/>
        <ant antfile="${sade.dest}/build.xml" inheritAll="false"/>
        <move2autodeploy source="${sade.dest}/build/"/>
    </target>

    <target name="tg-sade-connect" depends="prepare-dist-dir">
        <git repo="${tg-sade-connect.git}" branch="${tg-sade-connect.branch}"
            dest="${tg-sade-connect.dest}"/>
        <ant antfile="${tg-sade-connect.dest}/build.xml" inheritAll="false"/>
        <move2autodeploy source="${tg-sade-connect.dest}/build/"/>
    </target>

    <target name="tg-sade-defaultproject" depends="prepare-dist-dir">
        <git repo="${tg-sade-defaultproject.git}" branch="${tg-sade-defaultproject.branch}"
            dest="${tg-sade-defaultproject.dest}"/>
        <ant antfile="${tg-sade-defaultproject.dest}/build.xml" inheritAll="false"/>
        <move2autodeploy source="${tg-sade-defaultproject.dest}/build/"/>
    </target>

    <target name="sade-search" depends="prepare-dist-dir">
        <mkdir dir="${sade-search.dest}"/>
        <get src="${sade-search.build}" dest="${sade-search.dest}"/>
        <move2autodeploy source="${sade-search.dest}"/>
    </target>

    <!-- apply web.xml patch for dashboard, otherwise it will not be visible -->
    <target name="dashboard" depends="sade">
        <ant antfile="${build.dir}/sade/extensions/betterform/build.xml" target="patchWebXml" inheritAll="false"/>
    </target>

    <target name="projectapp" depends="prepare-dist-dir" if="projectapp.git"
        description="builds a project specific app that contains more specific files and sets up the db in a postinstall.xq">
        <git repo="${projectapp.git}" branch="${projectapp.branch}"
            dest="${projectapp.dest}"/>
        <ant antfile="${projectapp.dest}/build.xml" target="${projectapp.target}" inheritAll="false"/>
        <move2autodeploy source="${projectapp.dest}/build/"/>
    </target>

    <target name="build-deb-project" if="projectapp.git"
        depends="sade, dashboard-fix, junit-ant-patch, tg-sade-connect, tg-sade-defaultproject, sade-search, projectapp">
        <echo message="Successfully built a project specific version of SADE. Very well."/>
    </target>

    <target name="build-deb-generic" unless="projectapp.git"
        depends="sade, dashboard-fix, tg-sade-connect, tg-sade-defaultproject, sade-search">
        <echo message="Successfully built a generic version of SADE. Very well."/>
    </target>

    <target name="build-targz"/>

    <!-- ************************************* -->
    <!-- * ALL DEPENDENCIES BELOW THIS LINE! * -->
    <!-- ************************************* -->
    <target name="ant-dependencies" depends="install-ivy">
        <property name="ivy.default.ivy.user.dir" value="${basedir}/ivy"/>
        <ivy:retrieve conf="tasks"/>
        <path id="classpath">
            <fileset dir="./lib">
                <include name="*.jar"/>
            </fileset>
        </path>
        <!-- git -->
        <taskdef uri="antlib:com.rimerosolutions.ant.git"
            resource="com/rimerosolutions/ant/git/jgit-ant-lib.xml" classpathref="classpath"/>
        <!-- contrib has if -->
        <taskdef resource="net/sf/antcontrib/antcontrib.properties" classpathref="classpath"/>
        <!-- jdeb -->
        <taskdef name="deb" classname="org.vafer.jdeb.ant.DebAntTask" classpathref="classpath"/>
    </target>

    <target name="download-ivy" unless="skip.download">
        <mkdir dir="${ivy.jardir}"/>
        <get
            src="http://repo1.maven.org/maven2/org/apache/ivy/ivy/${ivy.version}/ivy-${ivy.version}.jar"
            dest="${ivy.jarfile}" usetimestamp="true"/>
    </target>

    <target name="install-ivy" depends="download-ivy" description="installs ivy">
        <path id="ivy.lib.path">
            <pathelement location="${ivy.jarfile}"/>
        </path>
        <taskdef resource="org/apache/ivy/ant/antlib.xml" uri="antlib:org.apache.ivy.ant"
            classpathref="ivy.lib.path"/>
    </target>

    <macrodef name="move2autodeploy">
        <attribute name="source"/>
        <sequential>
            <move todir="${build.dir}/sade/autodeploy/">
                <fileset dir="@{source}">
                    <include name="*.xar"/>
                </fileset>
            </move>
        </sequential>
    </macrodef>

    <macrodef name="git">
        <attribute name="repo"/>
        <attribute name="branch"/>
        <attribute name="dest"/>
        <sequential>
            <if>
                <available file="@{dest}" type="dir"/>
                <then>
                    <echo message="updating @{dest} from @{repo}"/>
                    <if>
                      <matches string="@{branch}" pattern="^tags/.*" />
                      <then>
                          <echo message="checking out tag: @{branch}"/>
                          <git:git directory="@{dest}" verbose="false">
                              <git:fetch uri="@{repo}" />
                              <git:checkout branchname="@{branch}" failonerror="true"/>
                          </git:git>
                      </then>
                      <else>
                          <echo message="pulling from branch: @{branch}"/>
                          <git-branch-checkout-pull repo="@{repo}" branch="@{branch}" dest="@{dest}"/>
                      </else>
                    </if>
                </then>
                <else>
                    <if>
                        <matches string="@{branch}" pattern="^tags/.*" />
                        <then>
                            <echo message="cloning @{repo} and checkout tag"/>
                            <git:git directory="@{dest}" verbose="false">
                                <git:clone uri="@{repo}"/>
                                <git:checkout branchname="@{branch}"/>
                            </git:git>
                        </then>
                        <else>
                            <echo message="cloning @{repo} to @{dest}"/>
                            <git:git directory="@{dest}" verbose="false">
                                <git:clone uri="@{repo}" branchtotrack="@{branch}"/>
                            </git:git>
                        </else>
                    </if>
                </else>
            </if>
        </sequential>
    </macrodef>

    <!-- the git:checkout task does not so well in checkount and setting new remote
         tracking branches, the following task is a workaround -->
    <macrodef name="git-branch-checkout-pull">
        <attribute name="repo"/>
        <attribute name="branch"/>
        <attribute name="dest"/>
        <sequential>

          <git:git directory="@{dest}" verbose="false">
              <git:fetch uri="@{repo}" />
              <git:branchlist outputfilename="@{dest}/git.branches"/>
          </git:git>

          <loadfile property="localBranchLine" srcfile="@{dest}/git.branches">
              <filterchain>
                  <linecontains>
                      <contains value="* @{branch}"></contains>
                  </linecontains>
              </filterchain>
          </loadfile>

          <if>
              <isset property="localBranchLine"/>
              <then>
                  <git:git directory="@{dest}" verbose="false">
                      <git:checkout branchname="@{branch}" failonerror="true" />
                      <git:pull uri="@{repo}" />
                  </git:git>
              </then>
              <else>
                  <echo message="no local branch existing yet, create and set remote tracking"/>
                  <git:git directory="@{dest}" verbose="false">
                      <git:checkout branchname="@{branch}" failonerror="true" createbranch="true" startpoint="@{branch}" />
                  </git:git>
                  <echo file="@{dest}/.git/config" append="true">
                  [branch "@{branch}"]
                          remote = origin
                          merge = refs/heads/@{branch}
                  </echo>
                  <git:git directory="@{dest}" verbose="false">
                      <git:pull uri="@{repo}" />
                  </git:git>
              </else>
          </if>

        </sequential>
    </macrodef>

</project>
