[![pipeline status](https://gitlab.gwdg.de/SADE/SADE/badges/develop/pipeline.svg)](https://gitlab.gwdg.de/SADE/build/commits/develop)

# SADE build targets

This repo contains build scripts for complete SADE instances.

The task is to collect all sources or builds and include them in a single
artifact `build/sade-VERSION.tar.gz`. All other artifacts are available as well.

# Requirements

`ant`

# Use!

Call `ant -f generic.xml` at the root folder should result in a deployable
instance within the `./build/`. There is a tarball available but for local tests
you can simply startup `./build/sade/bin/startup.sh` after build is completed.

You can download the latest artifact for the generic version [here](https://gitlab.gwdg.de/SADE/build/-/jobs/artifacts/develop/download?job=build).

# Customize!

To customize the instance please copy the file `generic.build.properties` to
`local.generic.build.properties` and point the property `sade.git` to your fork
of the [SADE/SADE](https://gitlab.gwdg.de/SADE/SADE) repo.

For specific (but may be older) cutomizations have a look on the ant build files
named with `fontane` or `neologie`. They include the generic build but
override some targets and comes with specific properties in the corresponding
`*.properties` files.

# Deploy!
There are Debain packages available at the [DARIAH-DE Aptly Repo](https://ci.de.dariah.eu/packages/pool/snapshots/s/sade/), published for
*all* distributions. They are currently untested and availably for the develop
branch only. Master will follow.
